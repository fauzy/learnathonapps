# LEARNATHON

Learning journey apps Mobile base

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

install :
1. NodeJS
2. Java JDK
3. Android SDK

```
npm install -g @ionic/cli
```


## Running the tests

Running the apps on your local
```
ionic serve
```

## Built With

* [NodeJS](https://nodejs.org/en/download/) 
* [JavaJDK](https://www.oracle.com/java/technologies/javase-downloads.html) 
* [Android SDK](https://developer.android.com/studio) - Software Development

* NodeJS version : v10.16.2
* npm : 6.9.0


## Ionic system Detail

* ionic (Ionic CLI) : 4.7.1
* Ionic Framework : IONIC 5.0.0
* Angular Verison : 8.3.25
* Angular Verison : 8.3.25

## Native Bridge

* capacitor/core : 1.5.0

