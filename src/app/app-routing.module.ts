import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ServiceGuard } from './service.guard';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./page/login/login.module').then( m => m.LoginPageModule),
    canActivate: [ServiceGuard]
  },
  {
    path: 'signup',
    loadChildren: () => import('./page/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./page/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./page/user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  },
  {
    path: 'courses-list',
    loadChildren: () => import('./page/courses-list/courses-list.module').then( m => m.CoursesListPageModule)
  },
  {
    path: 'courses-list-detail',
    loadChildren: () => import('./page/courses-list-detail/courses-list-detail.module').then( m => m.CoursesListDetailPageModule)
  },
  {
    path: 'modal-edit-profile',
    loadChildren: () => import('./page/modal-edit-profile/modal-edit-profile.module').then( m => m.ModalEditProfilePageModule)
  },
  {
    path: 'modal-search',
    loadChildren: () => import('./page/modal-search/modal-search.module').then( m => m.ModalSearchPageModule)
  },
  {
    path: 'daily-news',
    loadChildren: () => import('./page/daily-news/daily-news.module').then( m => m.DailyNewsPageModule)
  },
  {
    path: 'community',
    loadChildren: () => import('./page/community/community.module').then( m => m.CommunityPageModule)
  },
  {
    path: 'best-resources',
    loadChildren: () => import('./page/best-resources/best-resources.module').then( m => m.BestResourcesPageModule)
  },
  {
    path: 'people',
    loadChildren: () => import('./page/people/people.module').then( m => m.PeoplePageModule)
  },
  {
    path: 'inbox',
    loadChildren: () => import('./page/inbox/inbox.module').then( m => m.InboxPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./page/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./page/checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'inbox-detail',
    loadChildren: () => import('./page/inbox-detail/inbox-detail.module').then( m => m.InboxDetailPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./page/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'curses',
    loadChildren: () => import('./page/curses/curses.module').then( m => m.CursesPageModule)
  },
  {
    path: 'courses-play',
    loadChildren: () => import('./page/courses-play/courses-play.module').then( m => m.CoursesPlayPageModule)
  },
  {
    path: 'signup-socmed',
    loadChildren: () => import('./page/signup-socmed/signup-socmed.module').then( m => m.SignupSocmedPageModule)
  },
  {
    path: 'register-advance',
    loadChildren: () => import('./page/register-advance/register-advance.module').then( m => m.RegisterAdvancePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
