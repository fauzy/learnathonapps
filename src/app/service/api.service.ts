/**
 * @author Fauzi Agustian (arkanarafka.tech)
 * @class ApiService
 * @api v1
 * @description This is class for calling API Services and this is class for global other class. You dont need editing this class.
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl: any
  environments: any

  // Custome TOKEN & secretID
  clien_id : any = '5e501e7ba5ea87f6466116ea'
  client_secret  : any = 'ZQJ1Sxq2QW0FIOBAUFxfGxUW5vpaDkAeWi8YhByo8EGNyqf9t3'
  // custome access token
  access_token : any = 'UPb331yoD3QLgr5NKvg3sTGAKgREZVPNSUzIj7Gd'

  constructor() { 
    console.log("API service by fauzi");
    this.environments = 1
    this.loadServices()
  }

  setHeaderRequest() {
    const data_storage = JSON.parse(localStorage.getItem("access_token"));
    let token = data_storage.access_token
    const options = {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + token,
        "Lw-Client" : this.clien_id
      })
    }
    return options
  }
  setHeaderRequest_signup() {
    const options = {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.access_token,
        "Lw-Client" : this.clien_id
      })
    }
    return options
  }

  setHeaderRequestAuth() {
    const options = {
      headers: new HttpHeaders({
        "Lw-Client" : this.clien_id
      })
    }
    return options
  }

  async loadServices() {
    // production mode
    if (this.environments == 1) {
      this.apiUrl = 'https://api.learnworlds.com/'
      console.log('API running on dev production mode', this.apiUrl)
    }
    // dev mode
    else if (this.environments == 2) {
      this.apiUrl = 'https://api-010.learnworlds.com/'
      console.log('API running on development mode', this.apiUrl)
    }
  }

  Oauth2() {
    return this.apiUrl + 'oauth2/access_token'
  }
  sso() {
    return this.apiUrl + 'sso'
  }
  sso_2() {
    return this.apiUrl + 'user/sso'
  }

  registerService(){
    return this.apiUrl + 'users'
  }

  check_userID(){
    return this.apiUrl + 'user/'
  }
  get_allUSers(){
    return this.apiUrl + 'users'
  }

  get_all_courses(){
    return this.apiUrl + 'courses'
  }

  get_detail_courses(){
    return this.apiUrl + 'course'
  }

  edit_user(){
    return this.apiUrl + 'user/'
  }
  check_enrolled(){
    return this.apiUrl + 'users/course/'
  }

  enroll_user(){
    return this.apiUrl + 'user-product'
  }

}
