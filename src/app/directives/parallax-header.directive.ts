import { Directive, OnInit, ElementRef, Renderer2, Input, Renderer } from '@angular/core';
import { DomController } from '@ionic/angular';

@Directive({
  selector: '[appParallaxHeader]',
  host: {
    '(ionScroll)': 'onCntscroll($event)'
  }
})
export class ParallaxHeaderDirective implements OnInit {

 header : any;
 main_cnt : any
 ta : any

  constructor(
    private element: ElementRef,
    private renderer: Renderer,
    private domCtrl: DomController) {

  }

  ngOnInit() {
    let cnt = this.element.nativeElement.getElementByClassName('scroll-content')[0];
    this.header = cnt.getElementByClassName('main-content')[0];
    this.main_cnt = cnt.getElementByClassName('main-cnt');

    this.renderer.setElementStyle(this.header,'webTransformOrigin','center bottom');
    this.renderer.setElementStyle(this.header,'background-size','cover');
    this.renderer.setElementStyle(this.main_cnt,'position','absolute');

  }

  onCntscroll(ev) {
    console.log(ev)
    ev.domWrite(()=>{
      this.update(ev);
    })
  }

  update(ev){
    if (ev.scrollTop > 0) {
      this.ta = ev.scrollTop/2
    }
    this.renderer.setElementStyle(this.header,'webkitTransform','translate3d(0'+this.ta+'px,0) scale(1,1)')
  }


}
