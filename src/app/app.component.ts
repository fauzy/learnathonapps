import { Component } from '@angular/core';

import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuController } from '@ionic/angular';
// import {
//   Plugins,
//   StatusBarStyle,
// } from '@capacitor/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './service/api.service';

// const { StatusBar } = Plugins;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  isStatusBarLight = true
  user_id: any
  name: any
  email: any
  location : any
  loading_present : any
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private navCtrl: NavController,
    private menu: MenuController,
    public http: HttpClient,
    public apiService: ApiService,
    public loading: LoadingController,
    public alertCtrl : AlertController,
    public statusBar : StatusBar
  ) {
    this.initializeApp();
    this.check_userID()
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      // if (this.platform.is('ios')) {
      //   StatusBar.setStyle({
      //     style: StatusBarStyle.Dark
      //   });
      // } else if (this.platform.is('android')) {
      //   StatusBar.setStyle({
      //     style: StatusBarStyle.Dark
      //   });
      //   StatusBar.setBackgroundColor({
      //     color: '#26a1b2'
      //   })
      // }
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#26a1b2')
      // this.statusBar.styleLightContent()
    });
  }

  check_userID() {
    this.user_id = JSON.parse(localStorage.getItem('user_id'))
    console.log(this.user_id)
    if (this.user_id != undefined || this.user_id != null) {
      this.navCtrl.navigateRoot(['dashboard'])
      new Promise(resolve => {
        this.http.get(this.apiService.check_userID() + this.user_id + '/profile', this.apiService.setHeaderRequest())
          .toPromise()
          .then(response => {
            resolve(response)
            let res = []
            res.push(response)
            console.log('RES api firs time', res)
            this.name = res[0].user.username
            this.location = res[0].user.address
            

          })
          .catch((error: any) => {
            console.log(error)
          })
      })
    }
    else {
      this.navCtrl.navigateRoot(['login'])
    }
  }



  home() {
    this.navCtrl.navigateRoot(['dashboard'])
    this.menu.close()
  }
  profile() {
    this.navCtrl.navigateRoot(['user-profile'])
    this.menu.close()
  }
  async logout() {
    let alert = await this.alertCtrl.create({
      header : 'Info !',
      message : 'are you sure to logout now ?',
      buttons : [{
        text : 'No'
      },
      {
        text : 'Yes',
        handler : ()=>{
          this.loading_signout()
          localStorage.clear()
          setTimeout(() => {
            this.loading_present.dismiss()
            this.navCtrl.navigateRoot(['login'])
            this.menu.close()
          }, 1000);

        }
      }
    ] 
    })
    alert.present()
   

  }
  async loading_signout(){
    this.loading_present =  await this.loading.create({
      message: 'Signout...'
    })
    this.loading_present.present()
  }

  daily_news() {
    this.navCtrl.navigateForward(['daily-news'])
    this.menu.close()
  }
  community() {
    this.navCtrl.navigateForward(['community'])
    this.menu.close()
  }
  resource() {
    this.navCtrl.navigateForward(['best-resources'])
    this.menu.close()
  }
  people() {
    this.navCtrl.navigateForward(['people'])
    this.menu.close()
  }
  inbox() {
    this.navCtrl.navigateForward(['inbox'])
    this.menu.close()

  }
  about() {
    this.navCtrl.navigateForward(['about'])
    this.menu.close()

  }
}
