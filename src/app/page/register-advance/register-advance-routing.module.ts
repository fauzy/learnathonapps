import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterAdvancePage } from './register-advance.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterAdvancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterAdvancePageRoutingModule {}
