import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisterAdvancePage } from './register-advance.page';

describe('RegisterAdvancePage', () => {
  let component: RegisterAdvancePage;
  let fixture: ComponentFixture<RegisterAdvancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterAdvancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterAdvancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
