import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterAdvancePageRoutingModule } from './register-advance-routing.module';

import { RegisterAdvancePage } from './register-advance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterAdvancePageRoutingModule
  ],
  declarations: [RegisterAdvancePage]
})
export class RegisterAdvancePageModule {}
