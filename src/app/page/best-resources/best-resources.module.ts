import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BestResourcesPageRoutingModule } from './best-resources-routing.module';

import { BestResourcesPage } from './best-resources.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BestResourcesPageRoutingModule
  ],
  declarations: [BestResourcesPage]
})
export class BestResourcesPageModule {}
