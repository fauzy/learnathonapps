import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BestResourcesPage } from './best-resources.page';

describe('BestResourcesPage', () => {
  let component: BestResourcesPage;
  let fixture: ComponentFixture<BestResourcesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestResourcesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BestResourcesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
