import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BestResourcesPage } from './best-resources.page';

const routes: Routes = [
  {
    path: '',
    component: BestResourcesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BestResourcesPageRoutingModule {}
