import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-best-resources',
  templateUrl: './best-resources.page.html',
  styleUrls: ['./best-resources.page.scss'],
})
export class BestResourcesPage implements OnInit {
  user_email: any
  safeUrl: any
  url : any
  lazy : boolean = true

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public apiService: ApiService,
    public sanitizer: DomSanitizer
  ) {
    let email = localStorage.getItem('email')
    this.user_email = email.slice(1, -1)
   }

  ngOnInit() {
    this.check_sso()
    setTimeout(() => {
      this.lazy = false
    }, 3000);
  }

  check_sso() {
    let data_sso: any = {}
    data_sso.data = {}
    data_sso.email = this.user_email
    data_sso.redirectUrl = 'https://www.learnathonworld.com/workpad'
    new Promise(resolve => {
      this.http.post(this.apiService.sso(), data_sso, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          let res = []
          res.push(response)
          console.log('RESPONSE API SSO', res)
          this.url = res[0].url
          this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
          console.log("safe url", this.safeUrl)

        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

}
