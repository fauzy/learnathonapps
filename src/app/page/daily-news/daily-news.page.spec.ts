import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DailyNewsPage } from './daily-news.page';

describe('DailyNewsPage', () => {
  let component: DailyNewsPage;
  let fixture: ComponentFixture<DailyNewsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyNewsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DailyNewsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
