import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-inbox-detail',
  templateUrl: './inbox-detail.page.html',
  styleUrls: ['./inbox-detail.page.scss'],
})
export class InboxDetailPage implements OnInit {

  constructor(
    public navCtrl : NavController
  ) { }

  ngOnInit() {
  }

  onClick(){
    let curren_badge = localStorage.getItem('curren_badge')
    if(curren_badge == '-1' || curren_badge == '0'){
      localStorage.setItem('curren_badge','0')
    }else{
      let num_currentbadge = parseInt(curren_badge)
    let markread = num_currentbadge-1
    let markread_str = markread.toString(2)
    localStorage.setItem('curren_badge',markread_str)
    }
    
    this.navCtrl.back()
  }

}
