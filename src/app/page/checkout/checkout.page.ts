import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  entry: boolean = false
  data: any = {}
  course_id: any
  user_email: any
  item: any

  course_title: any
  course_des: any
  course_price: any
  course_img: any
  course_currency: any
  url : any
  safeUrl: any;
  lazy : boolean = true
  is_free : boolean = false
  courses_status : any
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public http: HttpClient,
    public apiService: ApiService,
    public loadingCtrl: LoadingController,
    public router: ActivatedRoute,
    public sanitizer: DomSanitizer,
  ) {
    this.url = this.router.snapshot.paramMap.get('url')
    console.log(this.url)
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    console.log(this.safeUrl)
    setTimeout(() => {
      this.lazy = false
      if(this.safeUrl){
        
      }
    }, 3500);
    let item = this.router.snapshot.paramMap.get('data_checkout')
    this.user_email = localStorage.getItem('email')
    console.log(this.user_email.slice(1,-1))
    this.item = JSON.parse(item)
    console.log(this.entry)
    console.log("data ckecout", this.item)
    this.course_title = this.item.title
    this.course_des = this.item.keywords
    this.course_price = this.item.price
    this.course_img = this.item.courseImage.url
    this.course_currency = this.item.currency

    this.courses_status = this.item.status
    if(this.courses_status == 'free'){
      this.is_free = true
    }
  }

  ngOnInit() {
  }

  async exit() {
    let alert = await this.alertCtrl.create({
      header: 'Info',
      message: 'You are sure to return to the homepage ?',
      buttons: [{
        text: 'No'
      },
      {
        text: 'Yes',
        handler: () => {
          this.navCtrl.navigateRoot(['dashboard'])
        }
      }
      ]
    })
    alert.present()
  }

  exit_(){
    this.navCtrl.navigateRoot(['dashboard'])
  }

  async checkout() {
    let loading = await this.loadingCtrl.create({
      message : 'Please wait...',
    })
    loading.present()
    // this.data.data = {}
    this.data.product = this.item.titleId
    this.data.justification = 'offline payment'
    this.data.user = this.user_email.slice(1,-1)
    this.data.price = this.item.price

    if (this.entry == false) {
      loading.dismiss()
      this.alert_error()
    }
    else {
      new Promise(resolve => {
        this.http.post(this.apiService.enroll_user(), this.data, this.apiService.setHeaderRequest())
          .toPromise()
          .then(response => {
            resolve(response)
            if (response) {
              loading.dismiss()
              let res = []
              res.push(response)
              console.log('RESPONSE API course', res)
              if(res[0].success == true){
                this.alert_success()
              }else{
                this.alert_error_reg()
              }
              

            }



          })
          .catch((error: any) => {
            console.log(error)
            loading.dismiss()
          })
      })
      
    }

  }

 async alert_success() {
    let alert = await this.alertCtrl.create({
      header : 'Success',
      message : 'Congratulations, you have successfully enrolled in this course please continue to start the course',
      buttons : [{
        text : 'Continue',
        handler : ()=>{
          this.navCtrl.navigateRoot(['dashboard'])
        }
      }]
    })

    alert.present()
  }

  async alert_error() {
    let alert = await this.alertCtrl.create({
      header: 'Oops',
      message: 'Please accept terms & conditions',
      buttons: [{
        text: 'OK',
      }
      ]
    })
    alert.present()
  }
  async alert_error_reg() {
    let alert = await this.alertCtrl.create({
      header: 'Oops',
      message: 'You has been Enrolled this courses',
      buttons: [{
        text: 'OK',
        handler : ()=>{
          this.navCtrl.navigateRoot(['dashboard'])
        }
      }
      ]
    })
    alert.present()
  }

  update() {
    console.log(this.entry)
  }

}
