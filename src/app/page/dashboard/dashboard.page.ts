import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  course_title: any
  course_desc: any
  course_img: any
  course_price: any
  user_id: any

  title_id: any

  badge: any
  item_message: any

  items_courses: any
  enrolled: boolean = false
  lazy: boolean = true
  no_enroll_items: any = []
  no_enrolled: boolean = false

  private_courses: any
  private_courses_enroled: any

  public_courses: any
  public_courses_enroled: any
  constructor(
    public navCtrl: NavController,
    public apiService: ApiService,
    public http: HttpClient
  ) {
    let user_id = localStorage.getItem('user_id')
    this.user_id = user_id.slice(1, -1)
    console.log('userid', this.user_id)
  }

  ngOnInit() {
    this.get_list()
    // this.getAllCourses()

  }
  ionViewDidEnter() {
    this.load_data_user()
  }
  ionViewWillEnter() {

  }


  getAllCourses() {
    let id_course = '5ab721e047d7dd19e28b457b'
    new Promise(resolve => {
      this.http.get(this.apiService.get_all_courses(), this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            this.lazy = false
          }
          let res = []
          res.push(response)
          // console.log('RESPONSE API course', res)
          let json_sring = JSON.stringify(res[0].courses)
          console.log("COURSES", res[0].courses)
          let json = res[0].courses
          var values = Object.keys(json).map(function (key) { return json[key]; });
          console.log("VALUE", values);
          this.items_courses = values
          // let res_json = json_sring.slice(28,-1)
          // console.log('title courses',res_json)
          // let final_json = JSON.parse(res_json)
          // console.log('final res',final_json)

          this.course_title = this.items_courses[0].title
          this.title_id = this.items_courses[0].titleId
          this.course_desc = this.items_courses[0].keywords
          this.course_img = this.items_courses[0].courseImage.url


        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

  load_data_user() {
    let user_id = JSON.parse(localStorage.getItem('user_id'))
    new Promise(resolve => {
      this.http.get(this.apiService.check_userID() + user_id + '/profile', this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          let res = []
          res.push(response)
          console.log('RES api Profile', res[0].user)
          let badge = res[0].user.badges_list.length
          let curren_badge = localStorage.getItem('curren_badge')
          if (curren_badge == undefined || curren_badge == null) {
            localStorage.setItem('curren_badge', badge)
            this.badge = localStorage.getItem('curren_badge')
          }
          else if (curren_badge == '0') {
            localStorage.setItem('curren_badge', '0')
            this.badge = localStorage.getItem('curren_badge')
          }
          else if (curren_badge < badge) {
            let new_badge = curren_badge + badge
            localStorage.setItem('curren_badge', new_badge)
            this.badge = localStorage.getItem('curren_badge')
          } else if (curren_badge == badge) {
            localStorage.setItem('curren_badge', badge)
            this.badge = curren_badge
          }



          this.item_message = res[0].user.badges_list
        })
        .catch((error: any) => {
          console.log(error)
        })
    })

  }

  courses_list() {
    this.navCtrl.navigateForward(['courses-list'])
  }
  more() {
    this.navCtrl.navigateForward(['courses-list'])
  }

  notif() {
    let data = {
      data_message: JSON.stringify(this.item_message)
    }
    this.navCtrl.navigateForward(['inbox', data])
  }
  detail() {
    let data = {
      title_id: this.title_id
    }
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + this.title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            if (array_users.length == 0) {
              console.log("USER NOT YET ENROLL")
              this.navCtrl.navigateForward(['courses-list-detail', data])
            }
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.enrolled = true
                this.navCtrl.navigateForward(['curses', data])
                break
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                this.navCtrl.navigateForward(['courses-list-detail', data])
              }
            }

          }

        })
        .catch((error: any) => {
          console.log(error)
        })
    })
    // this.navCtrl.navigateForward(['courses-list-detail', data])
  }


  //  get list courses

  get_list() {

    this.no_enrolled = false
    new Promise(resolve => {
      this.http.get(this.apiService.get_all_courses(), this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            this.lazy = false
          }
          let res = []
          res.push(response)
          console.log('RESPONSE API course', res)
          let json = res[0].courses
          var values = Object.keys(json).map(function (key) { return json[key]; });
          console.log("ALL Courses", values);
          this.items_courses = values
          for (let index = 0; index < this.items_courses.length; index++) {
            const element = this.items_courses[index];
            // this.check_enroll(element.titleId, index)

          }
          this.filterPrivateCourses()
          this.filterPublicCourses()
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }


  filterPrivateCourses() {
    this.private_courses = this.items_courses.filter(function (item) {
      return item.access === 'view-locked'
    });
    console.log('Private Courses', this.private_courses)
    for (let index = 0; index < this.private_courses.length; index++) {
      const element = this.private_courses[index];
      this.check_enroll_private(element.titleId, index)

    }
  }

  // show only private courses enrolled
  filterPrivateCoursesEnrolled() {
    this.private_courses_enroled = this.private_courses.filter(function (item) {
      return item.metadata.keywords === 'enrolled'
    });
    console.log('Only Private Courses enrolled', this.private_courses_enroled)

  }

  filterPublicCourses() {
    this.public_courses = this.items_courses.filter(function (item) {
      return item.access === 'public'
    });
    console.log('Public Courses', this.public_courses)
    for (let index = 0; index < this.public_courses.length; index++) {
      const element = this.public_courses[index];
      this.check_enroll_public(element.titleId, index)

    }
  }

  // public courses check enroll
  check_enroll_public(title_id, i) {
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.enrolled = true
                this.public_courses[i].metadata.keywords = 'enrolled'
                localStorage.setItem('is_enroled', '1')
                console.log("Public Courses Enrolled", this.public_courses)
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                // this.no_enrolled = true

              }
            }
          }
        })
        .catch((error: any) => {
          console.log(error)
        })

    })

  }

  // private courses check enroll
  check_enroll_private(title_id, i) {
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.enrolled = true
                this.private_courses[i].metadata.keywords = 'enrolled'
                localStorage.setItem('is_enroled', '1')
                console.log("Private Courses Enrolled", this.private_courses)
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                // this.no_enrolled = true

              }
            }

            this.filterPrivateCoursesEnrolled()
          }
        })
        .catch((error: any) => {
          console.log(error)
        })

    })

  }

  go_detail(titleId) {
    let data = {
      title_id: titleId
    }
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + titleId, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            if (array_users.length == 0) {
              console.log("USER NOT YET ENROLL")
              this.navCtrl.navigateForward(['courses-list-detail', data])
            }
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.navCtrl.navigateForward(['curses', data])
                break
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                this.navCtrl.navigateForward(['courses-list-detail', data])

              }
            }

          }

        })
        .catch((error: any) => {
          console.log(error)
        })
    })

  }
}
