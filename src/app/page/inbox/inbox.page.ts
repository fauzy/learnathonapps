import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage implements OnInit {
  items : any
  lazy : boolean = true
  constructor(
    public navCtrl : NavController,
    public router : ActivatedRoute,
    public http : HttpClient,
    public apiService : ApiService,
  ) {

   }

  ngOnInit() {
    this.items = JSON.parse(this.router.snapshot.paramMap.get('data_message'))
    console.log('list msg',this.items)
    this.load_data_user()
  }

  detail(){
    this.navCtrl.navigateForward('inbox-detail')
  }

  load_data_user() {
    let user_id = JSON.parse(localStorage.getItem('user_id'))
    new Promise(resolve => {
      this.http.get(this.apiService.check_userID() + user_id + '/profile', this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if(response){
            this.lazy = false
          }
          let res = []
          res.push(response)
          console.log('RES api Profile', res[0].user)
          console.log('list item',this.items)
          this.items = res[0].user.badges_list
        })
        .catch((error: any) => {
          console.log(error)
        })
    })

  }


}
