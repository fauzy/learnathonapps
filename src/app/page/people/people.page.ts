import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-people',
  templateUrl: './people.page.html',
  styleUrls: ['./people.page.scss'],
})
export class PeoplePage implements OnInit {
  name : any
  user_id : any

  item_user : any
  lazy : boolean = true
  constructor(
    public http : HttpClient,
    public apiService : ApiService,
    public loadingCtrl : LoadingController

  ) { }

  ngOnInit() {
    this.get_all_users()
  }

  get_all_users(){
    new Promise(resolve => {
      this.http.get(this.apiService.get_allUSers(), this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          if(response){
            this.lazy = false
          }
          resolve(response)
          let res = []
          res.push(response)
          this.item_user = res[0].users
          console.log('RES api', this.item_user)
          

        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

}
