import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ModalEditProfilePage } from '../modal-edit-profile/modal-edit-profile.page';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  user_id: any
  name: any
  email: any
  location: any
  country: any

  address: any
  phone: any
  website: any
  twitter: any
  instagram: any

  hours: any
  courses: any
  post: any
  achievment: any


  constructor(
    public modalCtrl: ModalController,
    public http: HttpClient,
    public apiService: ApiService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.load_data()
  }

  async edit_prof() {
    const modal = await this.modalCtrl.create({
      component: ModalEditProfilePage,
      componentProps: {
        name: this.name,
        country: this.country,
        phone: this.phone,
        address: this.address,
        website: this.website,
        twitter: this.twitter,
        instagram: this.instagram

      }
    })
    modal.present()
    modal.onDidDismiss()
      .then(data => {
        console.log(data)
        this.load_data();
      })
  }

  load_data() {
    this.user_id = JSON.parse(localStorage.getItem('user_id'))
    this.email = JSON.parse(localStorage.getItem('email'))
    new Promise(resolve => {
      this.http.get(this.apiService.check_userID() + this.user_id + '/profile', this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          let res = []
          res.push(response)
          console.log('RES api Profile', res)
          this.name = res[0].user.username
          this.location = res[0].user.address
          this.country = res[0].user.country
          this.name = res[0].user.username

          this.hours = res[0].user.stats.hours
          this.post = res[0].user.stats.posts
          this.phone = res[0].user.phone
          this.address = res[0].user.address
          this.website = res[0].user.website
          this.twitter = res[0].user.twitter
          this.instagram = res[0].user.instagram
          this.achievment = res[0].user.badges_list.length
          this.courses = res[0].user.courses.length


        })
        .catch((error: any) => {
          console.log(error)
        })
    })

  }

  inbox() {
    this.navCtrl.navigateForward(['inbox'])
  }
}
