import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalEditProfilePageRoutingModule } from './modal-edit-profile-routing.module';

// import { ModalEditProfilePage } from './modal-edit-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalEditProfilePageRoutingModule
  ],
  declarations: []
})
export class ModalEditProfilePageModule {}
