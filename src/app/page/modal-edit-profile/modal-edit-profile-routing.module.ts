import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalEditProfilePage } from './modal-edit-profile.page';

const routes: Routes = [
  {
    path: '',
    component: ModalEditProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalEditProfilePageRoutingModule {}
