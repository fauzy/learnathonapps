import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-modal-edit-profile',
  templateUrl: './modal-edit-profile.page.html',
  styleUrls: ['./modal-edit-profile.page.scss'],
})
export class ModalEditProfilePage implements OnInit {
  data: any = {}
  category: any
  user_id: any

  name: any
  country: any
  phone: any
  address: any
  website: any
  twitter: any
  instagram: any
  constructor(
    public modalCtrl: ModalController,
    public apiService: ApiService,
    public http: HttpClient,
    public loadinCtl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController
  ) {
  }

  ngOnInit() {
    console.log(this.name, this.address, this.phone)
    this.data.username = this.name
    this.data.country = this.country
    this.data.phone = this.phone
    this.data.address = this.address
    this.data.website = this.website
    this.data.twitter = this.twitter
    this.data.instagram = this.instagram

    this.category = 'info'
    this.user_id = JSON.parse(localStorage.getItem('user_id'))
  }
  segmentChanged(ev) {
    console.log('Segment changed', ev.detail.value);

  }
  async submit() {
    let loading = await this.loadinCtl.create({
      message: 'Changing data...'
    })
    loading.present()
    console.log(this.data)
    new Promise(resolve => {
      this.http.put(this.apiService.edit_user() + this.user_id, this.data, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            console.log('RES EDIT Profile', res)
            loading.dismiss()
            this.alert_()
          }


        })
        .catch((error: any) => {
          console.log(error)
          loading.dismiss()
        })
    })
  }

  async alert_() {
    let alert = await this.alertCtrl.create({
      header: 'Info !',
      message: 'Update Success..',
      buttons: [{
        text: 'OK',
        handler: () => {
          this.modalCtrl.dismiss()
        }
      }]
    })
    alert.present()
  }

  exit() {
    this.modalCtrl.dismiss()
  }

}
