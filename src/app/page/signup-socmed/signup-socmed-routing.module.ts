import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupSocmedPage } from './signup-socmed.page';

const routes: Routes = [
  {
    path: '',
    component: SignupSocmedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupSocmedPageRoutingModule {}
