import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-signup-socmed',
  templateUrl: './signup-socmed.page.html',
  styleUrls: ['./signup-socmed.page.scss'],
})
export class SignupSocmedPage implements OnInit {
  ionicForm: FormGroup;
  defaultDate = "1987-06-30";
  isSubmitted: boolean = true;
  data: any = {}
  avatar: any

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    private api: ApiService,
    private http: HttpClient,
    public formBuilder: FormBuilder,
    public router: ActivatedRoute,
    public appcom: AppComponent,
  ) { }

  ngOnInit() {
    let data_param = this.router.snapshot.paramMap.get('data');
    let data_socmed = this.router.snapshot.paramMap.get('socmed');
    let param = JSON.parse(data_param)
    console.log('from login', data_param)
    console.log('socmed', data_socmed)
    if (data_socmed == 'fb') {
      this.data.email = param.email
      this.data.username = param.name
      this.avatar = param.picture.data.url
    }
    else if(data_socmed == 'google'){
      this.data.email = param.email
      this.data.username = param.displayName
      this.avatar = param.imageUrl
    }


    this.ionicForm = this.formBuilder.group({
      first_name: new FormControl("", [Validators.required, Validators.minLength(3)]),
      email: new FormControl("", [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      username: new FormControl("", [Validators.required]),
      country: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.minLength(6)]),
    })
  }

  get errorControl() {
    return this.ionicForm.controls;
  }

  back() {
    this.navCtrl.pop()
  }

  async alert(content) {
    let alert = await this.alertCtrl.create({
      header: "Oops",
      message: content,
      buttons: [{
        text: 'OK',
        role: 'ok'
      }]
    })
    alert.present()
  }

  async alert_success() {
    let alert = await this.alertCtrl.create({
      header: "Success !",
      message: 'Congratulations you have successfully registered, proceed to login',
      buttons: [{
        text: 'OK',
        role: 'ok',
      }]
    })
    alert.present()
  }


  async submit() {
    this.isSubmitted = true;
    // if (!this.ionicForm.valid) {
    //   console.log(this.ionicForm.value)
    //   console.log(this.ionicForm.valid)
    //   console.log('Please provide all the required values!')
    //   return false;
    // } else {
    //   console.log(this.ionicForm.value)
    // }

    const loading = await this.loading.create({
      message: 'please wait...',
      spinner: 'lines',
      animated: true
    });
    await loading.present()
    let data_post = new FormData()
    data_post.append('username', this.data.username)
    data_post.append('email', this.data.email)
    data_post.append('password', this.data.password)
    data_post.append('country', this.data.country)
    data_post.append('avatar', this.avatar)
    console.log('DATA ', this.data)
    console.log('from data ', data_post)
    if (
      this.data.first_name == "" || this.data.first_name == undefined,
      this.data.email == "" || this.data.email == undefined,
      this.data.username == "" || this.data.username == undefined
    ) {
      loading.dismiss()
      let not_emp = 'There must not be an empty form, please check and try again'
      this.alert(not_emp)
    }
    else if (
      this.data.password != this.data.password_confirmation
    ) {
      loading.dismiss()
      let no_match = 'Passwords do not match, please try again'
      this.alert(no_match)
    }
    else {
      const options = {
        headers: new HttpHeaders().append('key', 'value')
      }
      new Promise(resolve => {
        // service API
        this.http.post(this.api.registerService(), data_post, this.api.setHeaderRequest_signup())
          .toPromise()
          .then(response => {
            if (response) {
              loading.dismiss()

            }
            resolve(response);
            let res = []
            res.push(response)
            if (res[0].success == true) {
              localStorage.setItem('user_id', JSON.stringify(res[0].user.id))
              localStorage.setItem('email', JSON.stringify(this.data.email))
              localStorage.setItem('user_detail', JSON.stringify(this.data))
              this.succes_regis()
              // this.alert_success()
              // this.navCtrl.navigateRoot('login')
            } else {
              let conten = 'Email or usernae already exists, please try again'
              this.alert(conten)
            }
            console.log("REST API", res)
          })
          .catch((error: any) => {
            loading.dismiss()
            let conten = 'Some thing wrong..please try again'
            this.alert(conten)
            console.log("DATA ERROR", error)
          })
      })
    }
  }

  async succes_regis(){
    let loading_success = await this.loading.create({
      message : 'syncing data...'
    })
    loading_success.present()
    // create token Oauth
    let data_post2 = new FormData()
    data_post2.append('client_id', '5e501e7ba5ea87f6466116ea')
    data_post2.append('client_secret', 'ZQJ1Sxq2QW0FIOBAUFxfGxUW5vpaDkAeWi8YhByo8EGNyqf9t3')
    data_post2.append('grant_type', 'client_credentials')
    new Promise(resolve=>{
      this.http.post(this.api.Oauth2(), data_post2, this.api.setHeaderRequestAuth())
        .toPromise()
        .then(response => {
          console.log("RES API", response)
          resolve(response);
          let res = []
          res.push(response)
          if (res[0].success == true) {
            localStorage.setItem('access_token', JSON.stringify(res[0].tokenData))
            loading_success.dismiss()
            this.appcom.check_userID()
            this.navCtrl.navigateForward('dashboard')
          }
        })
        .catch(err=>{
          console.log(err)
        })
    })
  }

}
