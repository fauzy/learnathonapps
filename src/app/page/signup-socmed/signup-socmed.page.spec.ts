import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SignupSocmedPage } from './signup-socmed.page';

describe('SignupSocmedPage', () => {
  let component: SignupSocmedPage;
  let fixture: ComponentFixture<SignupSocmedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupSocmedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupSocmedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
