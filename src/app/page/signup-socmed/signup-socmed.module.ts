import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignupSocmedPageRoutingModule } from './signup-socmed-routing.module';

import { SignupSocmedPage } from './signup-socmed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SignupSocmedPageRoutingModule
  ],
  declarations: [SignupSocmedPage]
})
export class SignupSocmedPageModule {}
