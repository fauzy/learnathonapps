import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-courses-play',
  templateUrl: './courses-play.page.html',
  styleUrls: ['./courses-play.page.scss'],
})
export class CoursesPlayPage implements OnInit {
  item : any
  course_title : any
  url : any
  safeUrl: any;
  lazy : boolean = true
  constructor(
    public router : ActivatedRoute,
    public sanitizer: DomSanitizer,
  ) { 
    this.course_title = this.router.snapshot.paramMap.get('tittel')
    this.url = this.router.snapshot.paramMap.get('url')
    console.log(this.url)
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    console.log(this.safeUrl)
    setTimeout(() => {
      if (this.safeUrl) {
      this.lazy = false
      }
    }, 3000);
    
  }

  ngOnInit() {
  }

}
