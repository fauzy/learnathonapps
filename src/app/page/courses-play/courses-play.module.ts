import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursesPlayPageRoutingModule } from './courses-play-routing.module';

import { CoursesPlayPage } from './courses-play.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursesPlayPageRoutingModule
  ],
  declarations: [CoursesPlayPage]
})
export class CoursesPlayPageModule {}
