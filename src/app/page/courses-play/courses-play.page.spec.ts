import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoursesPlayPage } from './courses-play.page';

describe('CoursesPlayPage', () => {
  let component: CoursesPlayPage;
  let fixture: ComponentFixture<CoursesPlayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesPlayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoursesPlayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
