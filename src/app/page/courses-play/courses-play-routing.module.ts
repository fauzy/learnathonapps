import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoursesPlayPage } from './courses-play.page';

const routes: Routes = [
  {
    path: '',
    component: CoursesPlayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursesPlayPageRoutingModule {}
