import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.page.html',
  styleUrls: ['./courses-list.page.scss'],
})
export class CoursesListPage implements OnInit {
  course_title: any
  course_desc: any
  course_img: any
  course_price: any
  course_currency: any
  registeredUsers: any
  courses_status: any
  title_id : any
  items_courses : any
  user_id : any
  enrolled : boolean = false
  no_enroll_items : any = []

  private_courses : any
  private_courses_enroled : any

  public_courses : any
  public_courses_enroled : any

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public apiService: ApiService
  ) { 
    let user_id = localStorage.getItem('user_id')
    this.user_id = user_id.slice(1, -1)
  }

  ngOnInit() {
    this.get_list()
  }

  get_list() {

    new Promise(resolve => {
      this.http.get(this.apiService.get_all_courses(), this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
          
          }
          let res = []
          res.push(response)
          console.log('RESPONSE API course', res)
          let json = res[0].courses
          var values = Object.keys(json).map(function (key) { return json[key]; });
          console.log("ALL Courses", values);
          this.items_courses = values
          for (let index = 0; index < this.items_courses.length; index++) {
            const element = this.items_courses[index];
            // this.check_enroll(element.titleId, index)

          }
          this.filterPrivateCourses()
          this.filterPublicCourses()
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }
  

  filterPrivateCourses() {
   this.private_courses = this.items_courses.filter(function (item) {
      return item.access === 'view-locked'
    });
    console.log('Private Courses', this.private_courses)
    for (let index = 0; index < this.private_courses.length; index++) {
      const element = this.private_courses[index];
      this.check_enroll_private(element.titleId, index)

    }
  }

  // show only private courses enrolled
  filterPrivateCoursesEnrolled() {
    this.private_courses_enroled = this.private_courses.filter(function (item) {
       return item.metadata.keywords === 'enrolled'
     });
     console.log('Only Private Courses enrolled', this.private_courses_enroled)
     
   }

  filterPublicCourses() {
    this.public_courses = this.items_courses.filter(function (item) {
       return item.access === 'public'
     });
     console.log('Public Courses', this.public_courses)
     for (let index = 0; index < this.public_courses.length; index++) {
      const element = this.public_courses[index];
      this.check_enroll_public(element.titleId, index)

    }
   }

   // public courses check enroll
  check_enroll_public(title_id, i) {
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.enrolled = true
                this.public_courses[i].metadata.keywords = 'enrolled'
                localStorage.setItem('is_enroled', '1')
                console.log("Public Courses Enrolled", this.public_courses)
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                // this.no_enrolled = true
              
              }
            }
          }
        })
        .catch((error: any) => {
          console.log(error)
        })

    })

  }

   // private courses check enroll
   check_enroll_private(title_id, i) {
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.enrolled = true
                this.private_courses[i].metadata.keywords = 'enrolled'
                localStorage.setItem('is_enroled', '1')
                console.log("Private Courses Enrolled", this.private_courses)
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                // this.no_enrolled = true
              
              }
            }

            this.filterPrivateCoursesEnrolled()
          }
        })
        .catch((error: any) => {
          console.log(error)
        })

    })

  }

  go_detail(titleId) {
    let data = {
      title_id : titleId
    }
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + titleId, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            if (array_users.length == 0){
              console.log("USER NOT YET ENROLL")
              this.navCtrl.navigateForward(['courses-list-detail',data])
            }
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                console.log("USER IS ENROLLED")
                this.navCtrl.navigateForward(['curses', data])
                break
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                this.navCtrl.navigateForward(['courses-list-detail',data])

              }
            }
  
          }
  
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
    
  }

  home() {
    this.navCtrl.navigateRoot(['dashboard'])
  }

  check_enroll(title_id,i){
    new Promise(resolve => {
      this.http.get(this.apiService.check_enrolled() + title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if (response) {
            let res = []
            res.push(response)
            let array_users = res[0].learners
            console.log('Response check', res[0].learners)
            let max_leng = array_users.length - 1
            for (let index = 0; index < array_users.length; index++) {
              const element = array_users[index];
              if (element.id == this.user_id) {
                this.enrolled = true
                console.log("USER IS ENROLLED")
                this.items_courses[i].metadata.keywords = 'enrolled'
                console.log("UPDATE VALUEE",this.items_courses)
               
                
              }
              if (index == max_leng) {
                console.log("USER NOT YET ENROLL")
                if(this.items_courses[i].access == 'public'){
                  this.no_enroll_items.push(this.items_courses[i])
                }
                console.log("NEW ITEMS no enroll",this.no_enroll_items)
              
              }
            }
  
          }
  
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
   }

}
