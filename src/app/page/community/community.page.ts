import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-community',
  templateUrl: './community.page.html',
  styleUrls: ['./community.page.scss'],
})
export class CommunityPage implements OnInit {
enrolled : boolean = false
lazy : boolean = true
items_courses : any
user_id : any
url : any
user_email : any
safeUrl : any
  constructor(
    public navCtrl : NavController,
    public http : HttpClient,
    public apiService : ApiService,
    public sanitizer : DomSanitizer
  ) {
    let user_id = localStorage.getItem('user_id')
    this.user_id = user_id.slice(1, -1)
    let email = localStorage.getItem('email')
    this.user_email = email.slice(1, -1)
   }

  ngOnInit() {
    // this.get_list()
    this.check_enroled()
    this.check_sso()
    
  }

  check_enroled(){
    let is_enroled = localStorage.getItem('is_enroled')
    console.log("is eronled ?",is_enroled)
    if (is_enroled == '1') {
      setTimeout(() => {
        this.lazy = false
      }, 3000);
      this.enrolled = true
    }else{
      setTimeout(() => {
        this.lazy = false
      }, 3000);
      this.enrolled = false
    }
  }

  enroll(){
    this.navCtrl.navigateForward('courses-list')
  }

  // get_list() {
  //   new Promise(resolve => {
  //     this.http.get(this.apiService.get_all_courses(), this.apiService.setHeaderRequest())
  //       .toPromise()
  //       .then(response => {
  //         resolve(response)
  //         if(response){
  //           this.lazy = false
  //         }
  //         let res = []
  //         res.push(response)
  //         console.log('RESPONSE API course', res)
  //         let json = res[0].courses
  //         var values = Object.keys(json).map(function (key) { return json[key]; });
  //         console.log("VALUE", values);
  //         this.items_courses = values
  //         // for (let index = 0; index < this.items_courses.length; index++) {
  //         //   const element = this.items_courses[index];
  //         //   this.check_enroll(element.titleId,index)
            
  //         // }
  
  //       })
  //       .catch((error: any) => {
  //         console.log(error)
  //       })
  //   })
  // }
  // check_enroll(title_id,i){
  //   new Promise(resolve => {
  //     this.http.get(this.apiService.check_enrolled() + title_id, this.apiService.setHeaderRequest())
  //       .toPromise()
  //       .then(response => {
  //         resolve(response)
  //         if (response) {
  //           let res = []
  //           res.push(response)
  //           let array_users = res[0].learners
  //           console.log('Response check', res[0].learners)
  //           if (array_users.length == 0){
  //             console.log("USER NOT YET ENROLL")
  //             this.enrolled = false
  //           }
  //           let max_leng = array_users.length - 1
  //           for (let index = 0; index < array_users.length; index++) {
  //             const element = array_users[index];
  //             if (element.id == this.user_id) {
  //               console.log("USER IS ENROLLED")
  //               this.enrolled = true
  //               break
               
                
  //             }
  //             if (index == max_leng) {
  //               console.log("USER NOT YET ENROLL")
  //               this.enrolled = false
  //             }
  //           }
  
  //         }
  
  //       })
  //       .catch((error: any) => {
  //         console.log(error)
  //       })
  //   })
  //  }

   check_sso(){
    let data_sso: any = {}
    data_sso.data = {}
    data_sso.email = this.user_email
    data_sso.redirectUrl = 'https://www.learnathonworld.com/social'
    new Promise(resolve => {
      this.http.post(this.apiService.sso(), data_sso, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          let res = []
          res.push(response)
          console.log('RESPONSE API SSO', res)
          this.url = res[0].url
          this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
          console.log("safe url",this.safeUrl)

        })
        .catch((error: any) => {
          console.log(error)
        })
    })
   }

}
