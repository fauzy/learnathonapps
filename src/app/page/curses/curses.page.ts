import { Component, OnInit, ElementRef, Renderer2, Input, Renderer } from '@angular/core';
import { DomController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { ThrowStmt } from '@angular/compiler';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/service/api.service';
import { RouterLinkActive, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-curses',
  templateUrl: './curses.page.html',
  styleUrls: ['./curses.page.scss'],
})
export class CursesPage implements OnInit {
  @Input('parallaxHeader') imagePath: string;
  @Input('parallaxHeight') parallaxHeight: number;
  private headerHeight: number;
  private header: HTMLDivElement;
  private mainContent: HTMLDivElement;
  header_show: boolean = false

  course_title: any
  course_desc: any
  course_img: any
  course_price: any
  course_currency: any
  registeredUsers: any
  courses_status: any

  courses_expire: any
  courses_exipire_type: any


  title_id: any
  item_checkout: any
  user_email: any

  constructor(

    private element: ElementRef,
    private renderer: Renderer2,
    private domCtrl: DomController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public http: HttpClient,
    public apiService: ApiService,
    public router: ActivatedRoute,
    private loadingCtrl : LoadingController

  ) { }

  ngOnInit() {
    this.title_id = this.router.snapshot.paramMap.get('title_id')
    let email = localStorage.getItem('email')
    this.user_email = email.slice(1, -1)
    console.log('title course', this.title_id)
    this.get_detail_courses()
    // this.headerHeight = this.parallaxHeight;
    console.log("header hight", this.headerHeight)
    this.mainContent = this.element.nativeElement.querySelector('.main-content');
    this.header = this.element.nativeElement.querySelector('.main-header');
    this.domCtrl.write(() => {
      this.header = this.renderer.createElement('div');
      this.renderer.insertBefore(this.element.nativeElement, this.header, this.element.nativeElement.firstChild);
      this.renderer.setStyle(this.header, 'background-image', 'url(' + this.imagePath + ')');
      this.renderer.setStyle(this.header, 'height', this.headerHeight + 'px');
      this.renderer.setStyle(this.header, 'background-size', 'cover');
    });
  }


  onCntscroll(ev) {

    this.domCtrl.read(() => {
      let translateAmt, scaleAmt;
      // Already scrolled past the point at which the header image is visible
      if (ev.detail.scrollTop > this.parallaxHeight) {
        return;
      }
      if (ev.detail.scrollTop >= 0) {
        translateAmt = -(ev.detail.scrollTop / 2);
        scaleAmt = 1;

      } else {
        translateAmt = 0;
        scaleAmt = -ev.detail.scrollTop / this.headerHeight + 0.3;

      }
      if (ev.detail.scrollTop >= 20) {
        this.header_show = true
        this.renderer.setStyle(this.mainContent, 'height', '150px');
      }
      else {
        this.header_show = false
        this.renderer.setStyle(this.mainContent, 'height', '200px');
      }
      this.domCtrl.write(() => {
        this.renderer.setStyle(this.header, 'transform', 'translate3d(0,' + translateAmt + 'px,0) scale(' + scaleAmt + ',' + scaleAmt + ')');
        this.renderer.setStyle(this.mainContent, 'transform', 'translate3d(0, ' + (-ev.detail.scrollTop) + 'px, 0');


      });
    });
  }

  back() {
    this.navCtrl.navigateBack(['dashboard'])
  }

  async enroll() {
    let alert = await this.alertCtrl.create({
      header: 'Info !',
      message: 'Are you sure to enroll this Courses ?',
      buttons: [{
        text: 'No'
      },
      {
        text: 'Yes',
        handler: () => {
          let data = {
            data_checkout: JSON.stringify(this.item_checkout)
          }
          this.navCtrl.navigateRoot(['checkout', data])
        }
      }
      ]

    })
    alert.present()
  }

  get_detail_courses() {
    new Promise(resolve => {
      this.http.get(this.apiService.get_detail_courses() + '/' + this.title_id, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          let res = []
          res.push(response)
          console.log('RESPONSE API course', res)
          this.item_checkout = res[0].course
          this.course_title = this.item_checkout.title
          this.course_desc = this.item_checkout.keywords
          this.course_img = this.item_checkout.courseImage.url
          this.course_price = this.item_checkout.price
          this.course_currency = this.item_checkout.currency
          this.registeredUsers = this.item_checkout.registeredUsers
          this.courses_status = this.item_checkout.status
          this.courses_expire = this.item_checkout.expires
          this.courses_exipire_type = this.item_checkout.expiresType

        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

  act_sso() {

  }

  async go_to_module() {
    let loading = await this.loadingCtrl.create({
      message : 'Please wait...'
    })
    loading.present()
    let data_sso: any = {}
    data_sso.data = {}
    data_sso.email = this.user_email
    data_sso.redirectUrl = 'https://www.learnathonworld.com/path-player?courseid=' + this.title_id + ''
    new Promise(resolve => {
      this.http.post(this.apiService.sso(), data_sso, this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          resolve(response)
          if(response){
            loading.dismiss()
          }
          let res = []
          res.push(response)
          console.log('RESPONSE API SSO', res)
          let url = res[0].url

          let data = {
            url: url,
            tittel : this.course_title

          }
          this.navCtrl.navigateForward(['courses-play', data])
        })
        .catch((error: any) => {
          console.log(error)
          loading.dismiss()
        })
    })
    // let data = {
    //   data_checkout: JSON.stringify(this.item_checkout)
    // }
    // this.navCtrl.navigateForward(['courses-play', data])
  }


}
