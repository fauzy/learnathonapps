import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CursesPage } from './curses.page';

describe('CursesPage', () => {
  let component: CursesPage;
  let fixture: ComponentFixture<CursesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CursesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CursesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
