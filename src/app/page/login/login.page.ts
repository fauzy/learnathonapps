import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { HttpClient } from '@angular/common/http';
import { resolve } from 'url';
import { AppComponent } from 'src/app/app.component';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  data: any = {}
  user_id: any
  username: any
  password: any
  isLoggedIn = false;
  users = { id: '', name: '', email: '', picture: { data: { url: '' } } };
  users_google : any

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public apiService: ApiService,
    public http: HttpClient,
    public appcom: AppComponent,
    private googlePlus: GooglePlus,
    private facebook: Facebook

  ) {


  }

  check_status_fb() {
    this.facebook.getLoginStatus()
      .then(res => {
        console.log("res", res)
        console.log(res.status);
        if (res.status === 'connect') {
          this.isLoggedIn = true;
          console.log("FB is connect")
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log("error login", e));
  }

  ionViewWillEnter() {
    let user_data = JSON.parse(localStorage.getItem('user_id'))
    let user_detail = JSON.parse(localStorage.getItem('user_detail'))
    console.log(user_data, user_detail)
    this.user_id = user_data
    // this.username = user_detail.username
    // this.password = user_detail.password

    console.log('user-id', this.user_id)
    console.log('password', this.password)

  }
  ngOnInit() {
    this.check_status_fb()
  }

  fb() {
    this.facebook.login(['public_profile', 'email'])
      // .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
      .then(res => {
        if (res.status === 'connected') {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));

    this.facebook.logEvent(this.facebook.EVENTS.EVENT_NAME_ADDED_TO_CART);
  }

  linkin() {

  }

  getUserDetail(userid: any) {
    this.facebook.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then(res => {
        console.log(res);
        this.users = res;
        let email = this.users.email
        let socmed = 'fb'
        this.check_user(email,socmed)
        // alert('RESPONSE'+res)
      })
      .catch(e => {
        console.log(e);
      });
  }

  google() {
    this.googlePlus.login({})
      .then(res => {
        console.log("RESPONSE GOOGLE", res)
        // alert('Response:' + JSON.stringify(res))
        let email = res.email
        this.users_google = res
        let socmed = 'google'
        this.check_user(email,socmed)
      }
      )
      .catch(err => {
        console.log("ERROR GOOGLE", err)
        alert('error:' + JSON.stringify(err))
      }


      );
  }

  signup() {
    // let data_cred : any = {}
    // data_cred.name = 'fauzi'
    // data_cred.email = 'aafas@asdas.com'
    // let push_data = JSON.stringify(data_cred)
    // let data = {
    //   data : push_data,
    //   email : 'faufafua@faul.com'
    // }
    // this.navCtrl.navigateForward(['signup-socmed',data])
    this.navCtrl.navigateForward(['signup'])
  }

  async login() {
    let loading = await this.loadingCtrl.create({
      message: 'loading please wait..'
    })
    loading.present()
    let data_post = new FormData()
    data_post.append('client_id', '5e501e7ba5ea87f6466116ea')
    data_post.append('client_secret', 'ZQJ1Sxq2QW0FIOBAUFxfGxUW5vpaDkAeWi8YhByo8EGNyqf9t3')
    data_post.append('grant_type', 'password')
    data_post.append('email', this.data.username)
    data_post.append('password', this.data.password)
    console.log(data_post)
    // api check user_id
    new Promise(resolve => {
      this.http.post(this.apiService.Oauth2(), data_post, this.apiService.setHeaderRequestAuth())
        // this.http.get(this.apiService.check_userID() + this.user_id + '/profile', this.apiService.setHeaderRequest())
        .toPromise()
        .then(response => {
          console.log("RES API", response)
          resolve(response);
          let res = []
          res.push(response)
          if (res[0].success == true) {
            // generate new access token
            let data_post2 = new FormData()
            data_post2.append('client_id', '5e501e7ba5ea87f6466116ea')
            data_post2.append('client_secret', 'ZQJ1Sxq2QW0FIOBAUFxfGxUW5vpaDkAeWi8YhByo8EGNyqf9t3')
            data_post2.append('grant_type', 'client_credentials')
            this.http.post(this.apiService.Oauth2(), data_post2, this.apiService.setHeaderRequestAuth())
              .toPromise()
              .then(response => {
                console.log("RES API", response)
                resolve(response);
                let res = []
                res.push(response)
                if (res[0].success == true) {
                  localStorage.setItem('access_token', JSON.stringify(res[0].tokenData))
                  // get user_id where email
                  setTimeout(() => {
                    this.http.get(this.apiService.get_allUSers(), this.apiService.setHeaderRequest())
                      .toPromise()
                      .then(response => {
                        console.log("RES API", response)
                        resolve(response);
                        let res = []
                        res.push(response)
                        if (res[0].success == true) {
                          for (let index = 0; index < res[0].users.length; index++) {
                            const element = res[0].users[index];
                            if (element.email == this.data.username) {
                              console.log("USERNAME FOUND")
                              localStorage.setItem('user_id', JSON.stringify(element.id))
                              localStorage.setItem('email', JSON.stringify(element.email))
                              loading.dismiss()
                              this.appcom.check_userID()
                              this.navCtrl.navigateForward('dashboard')

                              break
                            }
                          }
                        }
                        else {
                          // error with userid
                        }
                      })
                  }, 100);
                }

              })
              .catch((error: any) => {
                console.log(error)
                loading.dismiss()
                let conten = 'Some think wrong on server please try again'
                this.alert_error(conten)
              })
          }
          else {
            // error with userid
            loading.dismiss()
            let conten = 'The user credentials were incorrect. Wrong password or email, please try again !'
            this.alert_error(conten)
          }
        })
        .catch((error: any) => {
          console.log(error)
          loading.dismiss()
          let conten = 'Some think wrong on server please try again'
          this.alert_error(conten)
        })
    })
  }

  async check_user(email,socmed_type) {
    let loading = await this.loadingCtrl.create({
      message: 'loading please wait...'
    })
    loading.present()
    // generate new access token
    let data_post2 = new FormData()
    data_post2.append('client_id', '5e501e7ba5ea87f6466116ea')
    data_post2.append('client_secret', 'ZQJ1Sxq2QW0FIOBAUFxfGxUW5vpaDkAeWi8YhByo8EGNyqf9t3')
    data_post2.append('grant_type', 'client_credentials')
    new Promise(resolve => {
      this.http.post(this.apiService.Oauth2(), data_post2, this.apiService.setHeaderRequestAuth())
        .toPromise()
        .then(response => {
          console.log("RES API", response)
          resolve(response);
          let res = []
          res.push(response)
          if (res[0].success == true) {
            localStorage.setItem('access_token', JSON.stringify(res[0].tokenData))
            // api check user_id
            new Promise(resolve => {
              this.http.get(this.apiService.get_allUSers(), this.apiService.setHeaderRequest_signup())
                // this.http.get(this.apiService.check_userID() + this.user_id + '/profile', this.apiService.setHeaderRequest())
                .toPromise()
                .then(response => {
                  console.log("RES API", response)
                  resolve(response);
                  let res = []
                  res.push(response)
                  if (res[0].success == true) {
                    let max_leng = res[0].users.length - 1
                    for (let index = 0; index < res[0].users.length; index++) {
                      const element = res[0].users[index];
                      if (element.email == email) {
                        console.log("USERNAME FOUND")
                        localStorage.setItem('user_id', JSON.stringify(element.id))
                        localStorage.setItem('email', JSON.stringify(element.email))
                        loading.dismiss()
                        this.appcom.check_userID()
                        this.navCtrl.navigateForward('dashboard')
                        break
                      }

                      if (index == max_leng) {
                        if (socmed_type == 'fb'){
                          let data = {
                            data: JSON.stringify(this.users),
                            socmed : 'fb'
                          }
                          this.navCtrl.navigateForward(['signup-socmed', data])
                          console.log('NOT FOUND', index)
                          loading.dismiss()
                        }
                        else if(socmed_type == 'google'){
                          let data = {
                            data: JSON.stringify(this.users_google),
                            socmed : 'google'
                          }
                          this.navCtrl.navigateForward(['signup-socmed', data])
                          console.log('NOT FOUND', index)
                          loading.dismiss()
                        }
                        }
                       
                    }
                    // save userid

                  }
                  else {
                    // error with userid
                    loading.dismiss()
                    let conten = 'Some think wrong, please try again'
                    this.alert_error(conten)
                  }


                })
                .catch((error: any) => {
                  console.log(error)
                  loading.dismiss()
                  let conten = 'Some think wrong on server please try again'
                  this.alert_error(conten)
                })




            })
          }
        })
        .catch((error: any) => {
          console.log(error)
          loading.dismiss()
          let conten = 'Some think wrong on server please try again'
          this.alert_error(conten)
        })
    })


  }

  async alert_error(content) {
    let alert = await this.alertCtrl.create({
      header: 'Oops.!',
      message: content,
      buttons: [{
        text: 'OK'
      }]
    })
    alert.present()
  }

}
