import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoursesListDetailPage } from './courses-list-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CoursesListDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursesListDetailPageRoutingModule {}
