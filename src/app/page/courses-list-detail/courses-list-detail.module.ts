import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursesListDetailPageRoutingModule } from './courses-list-detail-routing.module';

import { CoursesListDetailPage } from './courses-list-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursesListDetailPageRoutingModule
  ],
  declarations: [CoursesListDetailPage]
})
export class CoursesListDetailPageModule {}
