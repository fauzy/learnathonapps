import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiService } from './service/api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParallaxHeaderDirective } from './directives/parallax-header.directive';
import { ModalEditProfilePage } from './page/modal-edit-profile/modal-edit-profile.page';
import { ModalSearchPage } from './page/modal-search/modal-search.page';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';





@NgModule({
  declarations: [AppComponent, ParallaxHeaderDirective, ModalEditProfilePage, ModalSearchPage],
  entryComponents: [
    ModalEditProfilePage,
    ModalSearchPage
  ],
  imports: [
    HttpClientModule,
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
  ],
  providers: [
    HttpClientModule,
    ApiService,
    StatusBar,
    SplashScreen,
    GooglePlus,
    Facebook,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
